define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dojo/text!app/widgets/example/example.html",
        "dojo/i18n!app/widgets/example/nls/local",
        "esri/layers/FeatureLayer",
        "esri/tasks/query",
        "app/services/exampleService"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        template,
        i18n,
        FeatureLayer,
        Query,
        exampleService
    ) {
        return declare([_WidgetBase, _TemplatedMixin], {
            templateString: template,
            i18n: i18n,
            startup() {
                this.inherited(arguments);

                this.initForm();
                this.fillDataTable();

            },
            initForm() {

                $('#date_example').datepicker({
                    // endDate: new Date(),
                    // startDate: new Date()
                });

                
                this.setValidator();

            },
            fillSelect() {

                //Using Promises
                exampleService.getStates().then((resp) => {
                    console.log("promises1------------");
                    console.log(resp);

                    data = resp.features.map(feature => ({
                        id: feature.attributes.nom_commune,
                        text: feature.attributes.nom_commune
                    }));

                    $('#select_commune').select2({
                        width: '100%',
                        placeholder: "Séléctionner une commune ...",
                        data: data,
                        allowClear: true
                    });

                }, (err) => {
                    console.log(err);
                });
                
                exampleService.getStates().then((resp) => {
                    console.log("promises2------------");
                    console.log(resp);
                });

                //Using Observables
                var observer1 = exampleService.getCities().subscribe(resp => {
                    console.log("observables1--------------");
                    console.log(resp);

                });

                var observer2 = exampleService.getCities().subscribe(resp => {
                    console.log("observables2--------------");
                    console.log(resp);

                });

            },
            setValidator() {
                var options = {
                    // Specify validation rules
                    excluded: ':disabled, :hidden, :not(:visible), [aria-hidden]',
                    rules: {
                        textbox_1: "required",
                        textbox_2: "required",
                        textbox_3: {
                            required: true,
                            number: true,
                            maxlength: 2
                        }
                    },
                    // Specify validation error messages
                    messages: {
                        textbox_1: i18n.requiredMessage,
                        textbox_2: i18n.requiredMessage,
                        textbox_3: {
                            required: i18n.requiredMessage,
                            number: i18n.validNumber,
                            maxlength: i18n.maxValue
                        },

                    }
                }

                $("#formID").validate(options);

                $(this.myButton).on("click", () => {
                    if ($("#formID").valid()) {
                        alert("Valid form");
                    }
                    this.fillSelect();
                });

            },
            fillDataTable() {
                var dataSet = [{
                        "first_name": "Airi",
                        "last_name": "Satou",
                        "position": "Accountant",
                        "office": "Tokyo",
                        "start_date": "28th Nov 08",
                        "salary": "$162,700"
                    },
                    {
                        "first_name": "Angelica",
                        "last_name": "Ramos",
                        "position": "Chief Executive Officer (CEO)",
                        "office": "London",
                        "start_date": "9th Oct 09",
                        "salary": "$1,200,000"
                    },
                    {
                        "first_name": "Ashton",
                        "last_name": "Cox",
                        "position": "Junior Technical Author",
                        "office": "San Francisco",
                        "start_date": "12th Jan 09",
                        "salary": "$86,000"
                    },
                    {
                        "first_name": "Bradley",
                        "last_name": "Greer",
                        "position": "Software Engineer",
                        "office": "London",
                        "start_date": "13th Oct 12",
                        "salary": "$132,000"
                    }

                ];


                var table = $('#example').DataTable({
                    data: dataSet,
                    responsive: true,
                    columns: [{
                            title: "first name",
                            data: "first_name"
                        },
                        {
                            title: "last name",
                            data: "last_name"
                        },
                        {
                            title: "position",
                            data: "position"
                        },

                        {
                            "render": function (data, type, row, meta) {
                                return `<div class="text-center">
                                    <button class="btn btn-outline-secondary btn-sm mb-1" data-action="edit"><i class="far fa-edit"></i></button>
                                    <button class="btn btn-outline-secondary btn-sm mb-1" data-action="delete"><i class="far fa-trash-alt"></i></button>
                                    </div>`;
                            }
                        }

                    ]
                });

                $('#example').on('click', '[data-action="edit"]', function (e) {

                    // when the table is in responsive mode the structure of the dom changes 
                    // so we need to check for the mode to specify the method on how to retrieve data

                    if ($(this).parents('tr').hasClass('child')) {
                        //responsive mode
                        console.log(table.row($(this).parents('tr').prev()).data()["salary"]);
                    } else {
                        //non responsive mode
                        console.log(table.row($(this).parents('tr')).data()["salary"]);
                    }
                    alert("edit");
                });

                $('#example').on('click', '[data-action="delete"]', function (e) {

                    if ($(this).parents('tr').hasClass('child')) {

                        console.log(table.row($(this).parents('tr').prev()).data()["first_name"]);
                    } else {
                        console.log(table.row($(this).parents('tr')).data()["first_name"]);
                    }
                    alert("delete");
                });
            }


        });
    });