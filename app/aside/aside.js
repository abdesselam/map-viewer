define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dojo/text!app/aside/aside.html",
        "dojo/i18n!app/aside/nls/local",
        "config/mapConfig",
        "config/layerConfig",
        "esri/dijit/BasemapGallery",
        "esri/dijit/Legend",
        "esri/dijit/LayerList"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        template,
        i18n,
        mapConfig,
        layerConfig,
        BasemapGallery,
        Legend,
        LayerList

    ) {
        return declare([_WidgetBase, _TemplatedMixin], {
            templateString: template,
            i18n: i18n,
            startup () {

                var basemapContainer = document.createElement("div");
                $(this.domNode).find("#basemapGallery").append(basemapContainer);

                //create basemap gallery to swith between basemaps
                var basemapGallery = new BasemapGallery({
                    showArcGISBasemaps: false,
                    basemaps: mapConfig.localBasemaps,
                    map: this.map
                }, basemapContainer);

                basemapGallery.startup();

                basemapGallery.on("error", (msg) =>{
                    console.log("basemap gallery error:  ", msg);
                });

                //select the starting basemap
                basemapGallery.select("algeria");

                //check for internet before adding online basemaps
                $.ajax({url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer"})
                .done(() => mapConfig.onlineBasemaps.forEach(basemap => basemapGallery.add(basemap)));

                var legendContainer = document.createElement('div');
                $(legendContainer).addClass("legend");
                $(this.domNode).find("#legend").append(legendContainer);

                //create legend widget to dispaly symbology of layers
                var legendDijit = new Legend({
                    map: this.map
                }, legendContainer);
                legendDijit.startup();

                var layers = document.createElement("div");
                $(this.domNode).find("#layerList").append(layers);

                //create layer list widget to display layers and have show/hide fonctionallity
                var layerList = new LayerList({
                    map: this.map,
                    layers: layerConfig.layers
                }, layers);
                layerList.startup();

            }
        });
    });