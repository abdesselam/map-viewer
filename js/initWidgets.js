define([
    "require",
    "config/widgetConfig",
    "js/loader"
], function (require, widgetConfig, loader) {

    return {
        endLoop: false,
        startup: function (map) {
             // First load all global widgets (header,aside, widgetContainer)
            require(["app/header/header", "app/aside/aside", "app/widgets/widgetContainer"], $.proxy(function (header, aside, widgetContainer) {
               //create an instance of header widget and add it's domNode (html code) to the documents in the #header element                 
                var headerWidget = new header();
                headerWidget.map = map;//this is added so you can access the map in the widget
                var headerNode = $(headerWidget.domNode);
                $('#header').append(headerNode);

                headerWidget.startup();

                //create an instance of aside widget and add it's domNode (html code) to the documents in the #main element       
                var asideWidget = new aside();
                asideWidget.map = map;//this is added so you can access the map in the widget
                var asideNode = $(asideWidget.domNode);
                $('#main').append(asideNode);

                asideWidget.startup();

                //loop throw widgetConfig and create widgets 
                for (let i = 0; i < widgetConfig.menus.length; i++) {

                    if (i >= (widgetConfig.menus.length - 1)) {
                        this.endLoop = true;
                    }

                    if (widgetConfig.menus[i].type == 'simple') {
                        //case of widget with a simple menu
                        this.simpleMenuWidget(widgetConfig.menus[i], widgetContainer, map);

                    } else if (widgetConfig.menus[i].type == 'dorpdown') {
                        //case of widget with a dropdown menu
                        this.dropdownMenuWidget(widgetConfig.menus[i], widgetContainer, map);
                    }

                }

            }, this));
        },
        simpleMenuWidget: function (menuConfig, widgetContainer, map) {
            // In the case of simple menu we only create a link without dropdown
            var menu = $('<li/>', {
                class: 'nav-item',
                html: '<a class="nav-link" href="#">' + menuConfig.icon + menuConfig.title + '</a>'

            }).appendTo('ul#menuList');
            require([menuConfig.widget.path], $.proxy(function (widget) {

                //create an instance of widgetcontainer for each widget and append the widget in it
                var widgetContainerCons = new widgetContainer();
                var widgetContainerNode = $(widgetContainerCons.domNode);
                widgetContainerNode.find('.widgetTitle .widgetIcon')[0].innerHTML = menuConfig.widget.icon;
                widgetContainerNode.find('.widgetTitle .widgetText')[0].innerHTML = menuConfig.widget.title;

                var widgetCons = new widget();
                var widgetNode = $(widgetCons.domNode);

                widgetContainerNode.find('.widgetBody').append(widgetNode);

                $('#main').append(widgetContainerNode);


                // attach a click event on the menu to display the widget
                menu.click(function (e) {
                    e.preventDefault();
                    $(widgetContainerCons.domNode).show();
                    if (widgetContainerCons.minimizedWidget) {
                        widgetContainerCons.restoreWidget();
                    }
                    //bring to front the widget on show
                    $('.widgetContainer').css('z-index', 40);
                    $(widgetContainerCons.domNode).css('z-index', 50);
                });
                widgetCons.map = map;//this is added so you can access the map in the widget
                widgetCons.startup();
                widgetContainerCons.startup();

                // check if all widget are loaded and run onload function
                if (this.endLoop) {
                    loader.appLoaded = true
                    if (loader.windowLoaded) {
                        loader.onLoad();
                    }

                }
            }, this));
        },
        dropdownMenuWidget: function (menuConfig, widgetContainer, map) {

            // In the case of dropdown menu we need to create a list and add links to widgets 
            var dropdown = $('<li/>', {
                class: 'nav-item dropdown',
                html: '<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + menuConfig.icon + menuConfig.title + '</a>'

            }).appendTo('ul#menuList');

            var dropdownMenu = $('<div/>', {
                class: 'dropdown-menu',
                "aria-labelledby": "navbarDropdown",

            }).appendTo(dropdown);

            //loop throw submenus and create widgets
            for (let i = 0; i < menuConfig.submenus.length; i++) {

                require([menuConfig.submenus[i].widget.path], $.proxy(function (widget) {

                    //create an instance of widgetcontainer for each widget and append the widget's Node in it
                    var widgetContainerCons = new widgetContainer();
                    var widgetContainerNode = $(widgetContainerCons.domNode);
                    widgetContainerNode.find('.widgetTitle .widgetIcon')[0].innerHTML = menuConfig.submenus[i].widget.icon;
                    widgetContainerNode.find('.widgetTitle .widgetText')[0].innerHTML = menuConfig.submenus[i].widget.title;

                    var widgetCons = new widget();
                    var widgetNode = $(widgetCons.domNode);

                    widgetContainerNode.find('.widgetBody').append(widgetNode);

                    $('#main').append(widgetContainerNode);

                    var menu = $('<a/>', {
                        class: 'dropdown-item',
                        href: "#",
                        html: menuConfig.submenus[i].icon + menuConfig.submenus[i].title

                    }).appendTo(dropdownMenu);

                     // attach a click event on the menu to display the widget
                    menu.click(function (e) {
                        e.preventDefault();
                        $(widgetContainerCons.domNode).show();
                        if (widgetContainerCons.minimizedWidget) {
                            widgetContainerCons.restoreWidget();
                        }
                        //bring to front the widget on show
                        $('.widgetContainer').css('z-index', 40);
                        $(widgetContainerCons.domNode).css('z-index', 50);
                    });

                    widgetCons.map = map;//this is added so you can access the map in the widget

                    widgetCons.startup();
                    widgetContainerCons.startup();

                    // check if all widget are loaded and run onload function
                    if (this.endLoop) {
                        if (i >= (menuConfig.submenus.length - 1)) {
                            loader.appLoaded = true;
                            if (loader.windowLoaded) {
                                loader.onLoad();
                            }
                        }
                    }
                }, this));
            }

        }
    }


});