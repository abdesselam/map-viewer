define([
    "dojo/i18n!config/nls/local",
    "esri/layers/FeatureLayer",
    'esri/dijit/PopupTemplate'    

], function (i18n, FeatureLayer, PopupTemplate) {

    return {

        layers: [

            new FeatureLayer('http://10.5.28.48:6080/arcgis/rest/services/viewer/mxd_viewer/FeatureServer/0', {
                title: 'Line',
                id: 'line',
                mode: FeatureLayer.MODE_ONDEMAND,
                outFields: ["*"],
                infoTemplate: new PopupTemplate({
                    title: "{objectid}",
                    description: "{label}"
                })
            })
        ]

    }
});